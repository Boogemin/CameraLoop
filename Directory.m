%% Get save directories
function [Powder Part String_Pow String_Part] = Directory (Powderfoldername, Partfoldername, folder, Parentfoldername);
if Powderfoldername == 0
    Powder = 0;
    Part = 0;
    String_Pow = 0;
    String_Part = 0;
    return;
else
Folder = strcat(folder, '\', Parentfoldername, '\');
mkdir (Folder, Powderfoldername); 
mkdir (Folder, Partfoldername);   
Powder = Powderfoldername;
Part    = Partfoldername;
String_Pow = strcat(Folder, Powder);
String_Part= strcat(Folder, Part);
end
end
