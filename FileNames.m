%% SaveFile
function [Powderfoldername Partfoldername folder Parentfoldername] = FileNames();
clear all;close all; clc
Prompt = {'Name of Parent Folder:', 'Name of Powder Bed Folder:', 'Name of Part Folder:' };
dlg_title = 'Folder Names';
num_lines = 1;
defaultans= {'PartName','Powder', 'Part'};
Input = inputdlg(Prompt,dlg_title,[1 50; 1 50; 1 50],defaultans);
if isempty(Input) == 1
    Powderfoldername = 0
    Partfoldername = 0
    folder = 0
    Parentfoldername = 0
    return
else
Parentfoldername = Input{1,:};
folder = uigetdir;
mkdir(folder,Parentfoldername)
Powderfoldername = Input{2,:};
Partfoldername   = Input{3,:};
end
end
