function Write(Parentfoldername, Powder, Part, String_Pow, String_Part, frame, i);
    B = mod(i,2);
       if B == 1;
        Count1 = (i+1)/2;
        filename1 = [Parentfoldername sprintf('_Powder_%06d', Count1) '.jpg'];
        SaveFile1= String_Pow;
        cd(SaveFile1)
        imwrite(frame,filename1)
        clear frame
        return
    else
        Count2 = i/2;
        SaveFile2= String_Part;
        filename2= [Parentfoldername sprintf('_Part_%06d', Count2) '.jpg'];
        cd(SaveFile2)
        imwrite(frame,filename2);
        clear frame
        return
    end
  
end
