%% Initilization
function [vid] = Initilization ();
    imaqreset
    clc
    vid=videoinput('pixelinkimaq');
    src=getselectedsource(vid);
    %set(src,'ExposureMode','Auto');
    set(src,'ColorTemperatureMode', 'off')
    set(src,'GammaMode','on')
    vid.FramesPerTrigger = inf;
    triggerConfigs = triggerinfo(vid);
    triggerconfig(vid,triggerConfigs(3));
    
    
end