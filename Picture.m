%% Get a SnapShot
function [frame] = Picture(vid)

        start(vid);
        while get(vid,'FramesAvailable')<1;
            unavailable = 1;
        end
        frame = getdata(vid,1); %take a picture
        stop(vid)
        flushdata(vid)
        frame = double(frame)/(2^10);
        
end