%% Run Camera
function [] = RunCamera();
[Powderfoldername Partfoldername folder Parentfoldername] = FileNames();
[vid] = Initilization ();
[Powder Part String_Pow String_Part] = Directory (Powderfoldername, Partfoldername, folder, Parentfoldername);
Cameraloop(Parentfoldername, Powder, Part, String_Pow, String_Part, vid);
end